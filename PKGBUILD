# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Timothy Gu <timothygu99@gmail.com>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Tom Gundersen <teg@jklm.no>
# Contributor: Thomas Baechler <thomas@archlinux.org>
# Contributor: Ivan Shapovalov <intelfx@intelfx.name>

pkgname=libfprint-primex
_pkgname=libfprint
pkgver=1.90.2.r34.gb80af1b
pkgrel=1
pkgdesc="Library for Pixelauth PrimeX fingerprint readers"
url="https://fprint.freedesktop.org/"
arch=(x86_64)
license=(LGPL)
depends=(libgusb pixman nss systemd-libs)
makedepends=(git meson gtk-doc gobject-introspection systemd)
checkdepends=(python python-cairo python-gobject 'umockdev>=0.13.2')
provides=(libfprint libfprint-2.so)
conflicts=(libfprint libfprint-git)
groups=(fprint)
source=("git+https://gitlab.freedesktop.org/llforce/libfprint.git#commit=b80af1bb")
sha256sums=('SKIP')

pkgver() {
  cd $_pkgname
  git describe --tags | sed 's/^V_\|^v//;s/_/./g;s/-/.r/;s/-/./'
}

prepare() {
  cd $_pkgname

  # fix typo
  sed -i -e 's|/usr/lib/fprintd/pa-storage.variant|/var/lib/fprint/pa-storage.variant|'g libfprint/drivers/pixelauth/storage_helper.h 
}

build() {
  arch-meson $_pkgname build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  mkdir -p $pkgdir/var/lib/fprint/
  touch $pkgdir/var/lib/fprint/.libfprint
}